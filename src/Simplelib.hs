
module Simplelib
where
  import Prelude hiding (signum, max, enumFromTo)

  inc :: Int -> Int
  inc x = x + 1

  -- square :: Num a => a -> a
  square x = x * x

  cube :: Num a => a -> a
  cube x = x * square x
  
  showResult x = "The result is " ++ show (x*x)

  signum :: (Ord a, Num a) => a -> Int
  signum x 
    | x < 0 = -1
    | x > 0 = 1
    | otherwise = 0

  sort2 :: Ord a => a -> a -> (a, a)
  sort2 a b
    | a < b = (a, b)
    | a > b = (b, a)
    | otherwise  = (a, b)

  almostEqual (x1, y1) (x2, y2)
    | (x1 == x2) = (y1 == y2) 
    | (x1 == y2) = (y1 == x2) 
    | otherwise  = False

  almostEqualSw pair1 pair2 = 
    (pair1 == pair2) || (swap pair1 == pair2) where 
      swap (x,y) = (y,x)

  -- isLower :: Char -> Bool
  isLower c = elem c ['a'..'z']

  mangle :: String -> String
  mangle (i:w) = w ++ [i] 
  mangle "" = ""
  
  -- returns a list with all multiples of n(umber) until l(imit) => multiples 3 10 = [3, 6, 9]
  multiples n l = [n, n*2..l]
  divide dd dr = length (multiples dr dd)

  len [] = 0
  len (x:xs) = 1 + (len xs)

  enumFromTo m n
    | m == n = [m]
    | m > n = []
    | otherwise = m : enumFromTo (m+1) n

  countOdds [] = 0
  countOdds (x:xs)
    | x `mod` 2 == 1 = 1 + countOdds xs
    | otherwise = countOdds xs

  removeOdds [] = []
  removeOdds (x:xs)
    | x `mod` 2 == 1 = removeOdds xs
    | otherwise = x : removeOdds xs
    