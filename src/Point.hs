module Point
where
  -- import Prelude hiding (signum, max)

  type Point = (Int, Int)

  origin :: Point
  origin = (0, 0)

  moveR :: Point -> Int -> Point
  moveR (x, y) d = (x+d, y)

  moveU (x, y) d = (x, y+d)

  move (x, y) dx dy = (x+dx, y+dy)

  -- compute distance between two points
  distance :: Point -> Point -> Float
  distance (x1, y1) (x2, y2) = 
    sqrt(fromIntegral (dx*dx + dy*dy)) 
      where
        dx = x2 - x1
        dy = y2 - y1
      
  
  -- UNEFFICIENT!!!
  -- Closest Point: given a point and a list of points, get the closest point
  closestPoint :: Point -> [Point] -> Point 
  closestPoint p [] = p -- no points
  closestPoint p [px] = px -- only one point
  closestPoint p (px:pxs) = closestOfTwo p px (closestPoint p pxs)

  -- comparing two by two, handy helper : )
  closestOfTwo p p1 p2 
    | distance p p1 < distance p p2 = p1
    | otherwise = p2

  -- let points = [(1, 1), (-3, 5), (3, 4), (3, 1), (6, 4)]
  