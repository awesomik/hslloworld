module Lib
where
  import Data.List

  someFunc :: IO ()
  someFunc = putStrLn "someFunc"

  -- almostIncSeq [1, 3, 2, 1] -> False
  -- almostIncSeq [1, 3, 2] -> true  ([1, 3], [1, 2])
  almostIncreasingSequence s = and $ (<2) . length . filter (uncurry (>=)) . zip s . tail <$> [s, tail s]


  isListIncr [] = True
  isListIncr [x] = True
  isListIncr (h:e:r) = h <= e && isListIncr (e:r)

  reverseSort l = reverse . sort $ l -- first applies sort, then reverse => reverse(sort(l))
  rSortFn l = reverse(sort l)

  -- repmN 2 [0, 3, 5] = [0, 0, 3, 3, 5, 5]
  repN n l = concat [ rep n x | x <- l]
    where 
      rep n x
        | n == 0 = []
        | otherwise = x : rep (n-1) x 

  rmvOddPos l = rmvEl l 0
    where
      rmvEl [] n = []
      rmvEl (x:xs) n 
        | (mod n 2) == 0 = rmvEl xs (n+1)
        | otherwise = x : rmvEl xs (n+1)
        