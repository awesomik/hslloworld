module Recursion
where
  -- import Prelude hiding (min)
  import Data.Char

  sumup [] = 0
  sumup (x:l) = x + sumup l

  -- sumNat :: Num p => p -> p
  sumNat 0 = 0
  sumNat n = n + sumNat (n-1)

  -- better...
  natSum 0 = 0
  natSum x 
    | x > 0 = x + natSum (x-1)
    | otherwise = error "Only N numbers"

  repeatN elem n
    | n <= 0 = []
    | otherwise = elem : repeatN elem (n-1)

  -- suffixes "Hello"  ⇒  ["Hello", "ello", "llo", "lo", "o"]
  suffixes s
    | s == "" = []
    | otherwise = s:suffixes (tail s)

  allUpper [] = []
  allUpper (x:xs) = (toUpper x) : allUpper xs

  -- extractDigits "das43 dffe 23 5 def45" -> "4323545"
  extractDigits [] = []
  extractDigits (x:xs)
    | isDigit(x) = x : extractDigits xs
    | otherwise = extractDigits xs

  accumProd [] = 0
  accumProd [x] = x
  accumProd (x:xs) = x * accumProd xs
  
  minOfList [] = 0 -- maxBound gives error
  minOfList (x:[]) = x
  minOfList (x:xs) = x `min` (minOfList xs) -- minimum of x and the rest of the list

  flatten [] = []
  flatten (x:xs) = x ++ flatten xs

  -- sum of all even elements of a list => sumEvens [2, 6, 9, 3, 4] -> 12
  sumEvens [] = 0
  sumEvens (x:xs)
    | x `mod` 2 == 0 = x + (sumEvens xs)
    | otherwise = sumEvens xs

  -- sum of square roots of all positive numbers in a list
  sumSqroot [] = 0
  sumSqroot (x:xs)
    | x > 0 = x + (sumSqroot xs)
    | otherwise = sumSqroot xs

  rev [] = []
  rev (x:xs) = rev xs ++ [x]

  absList [] = []
  absList (x:xs) 
    | x < 0 = ((-1) * x) : absList xs
    |otherwise = x : absList xs

  -- integral :: Int -> Int -> [Int] -> [Int] -> [Double]
  -- integral l r a b 